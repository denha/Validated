<?php

declare(strict_types=1);

namespace denha\Validated\Divertor;

use denha\Validated\Annotations\Past as PastStorag;
use denha\Validated\Exception\ValidatedException;
use denha\Validated\Handle;

class Past extends Handle
{

    /** @var PastStorag */
    private $Past;

    public function __construct(?PastStorag $PastStorag)
    {
        $this->Past = $PastStorag;
    }

    public function handleRequest($property, &$value)
    {
        if (!$this->Past && null != $this->getSuccessor()) {
            return $this->getSuccessor()->handleRequest($property, $value);
        } elseif (!$this->Past) {
            return;
        }

        if (null !== $value) {
            $value = (string)$value;
            if (strlen($value) && (strtotime($value) === false || strtotime($value) > time())) {
                throw new ValidatedException($this->Past->message ?: sprintf('[%s] 必须是一个正确的过期日期', $property->getName()));
            }
        }

        // 转发给后继的责任对象
        if (null != $this->getSuccessor()) {
            return  $this->getSuccessor()->handleRequest($property, $value);
        }
    }
}
