<?php

declare(strict_types=1);

namespace denha\Validated\Divertor;

use denha\Validated\Annotations\Date as DateStorag;
use denha\Validated\Exception\ValidatedException;
use denha\Validated\Handle;

class Date extends Handle
{
    /** @var DateStorag */
    private $Date;

    public function __construct(?DateStorag $DateStorag)
    {
        $this->Date = $DateStorag;
    }

    public function handleRequest($property, &$value)
    {

        if (!$this->Date && null != $this->getSuccessor()) {
            return $this->getSuccessor()->handleRequest($property, $value);
        } elseif (!$this->Date) {
            return;
        }

        if(null === $value){
            $value = $value;
        } 
        // 时间戳
        else if (is_numeric($value)) {
            $value = date($this->Date->foramt, $value);
        }
        // 日期格式
        else if (is_string($value) && strlen($value) && ($time = strtotime($value)) !== false) {
            $value = date($this->Date->foramt, $time);
        }

        if(false === $value){
            throw new ValidatedException($this->Future->message ?: sprintf('[%s] 必须是一个正确的日期', $property->getName()));
        }


        // 转发给后继的责任对象
        if (null != $this->getSuccessor()) {
            return $this->getSuccessor()->handleRequest($property, $value);
        }
    }
}
