<?php

declare(strict_types=1);

namespace denha\Validated\Divertor;

use denha\Validated\Annotations\Future as FutureStorag;
use denha\Validated\Exception\ValidatedException;
use denha\Validated\Handle;

class Future extends Handle
{

    /** @var FutureStorag */
    private $Future;

    public function __construct(?FutureStorag $FutureStorag)
    {
        $this->Future = $FutureStorag;
    }

    public function handleRequest($property, &$value)
    {
        if (!$this->Future && null != $this->getSuccessor()) {
            return $this->getSuccessor()->handleRequest($property, $value);
        } elseif (!$this->Future) {
            return;
        }

        if (null !== $value) {
            $value = (string)$value;
            if (strlen($value)  && (strtotime($value) === false || strtotime($value) < time())) {
                throw new ValidatedException($this->Future->message ?: sprintf('[%s] 必须是一个正确的未来日期', $property->getName()));
            }
        }

        // 转发给后继的责任对象
        if (null != $this->getSuccessor()) {
            return $this->getSuccessor()->handleRequest($property, $value);
        }
    }
}
