<?php

declare(strict_types=1);

namespace denha\Validated\Divertor;

use denha\Validated\Annotations\NotBlank as NotBlankStorag;
use denha\Validated\Exception\ValidatedException;
use denha\Validated\Handle;

class NotBlank extends Handle
{

    /** @var NotBlankStorag */
    private $NotBlank;

    public function __construct(?NotBlankStorag $NotBlankStorag)
    {
        $this->NotBlank = $NotBlankStorag;
    }

    public function handleRequest($property, &$value)
    {
        if (!$this->NotBlank && null != $this->getSuccessor()) {
            return $this->getSuccessor()->handleRequest($property, $value);
        } elseif (!$this->NotBlank) {
            return;
        }

        $value = is_string($value) ? trim($value) : $value;
        if ((is_string($value) && strlen($value) === 0) || (!is_string($value) && !is_numeric($value) && !$value)) {
            throw new ValidatedException($this->NotBlank->message ?: sprintf('[%s] 不能为空', $property->getName()));
        }

        // 转发给后继的责任对象
        if (null != $this->getSuccessor()) {
            return $this->getSuccessor()->handleRequest($property, $value);
        }
    }
}
