<?php

declare(strict_types=1);

namespace denha\Validated\Divertor;

use denha\Validated\Annotations\NotString as NotStringStorag;
use denha\Validated\Exception\ValidatedException;
use denha\Validated\Handle;

class NotString extends Handle
{

    /** @var NotStringStorag */
    private $NotString;

    public function __construct(?NotStringStorag $NotStringStorag)
    {
        $this->NotString = $NotStringStorag;
    }

    public function handleRequest($property, &$value)
    {
        if (!$this->NotString && null != $this->getSuccessor()) {
            return $this->getSuccessor()->handleRequest($property, $value);
        } elseif (!$this->NotString) {
            return;
        }
        
        if (is_string($value)) {
            throw new ValidatedException($this->NotString->message ?: sprintf('[%s] 不能是字符串类型', $property->getName()));
        }

        // 转发给后继的责任对象
        if (null != $this->getSuccessor()) {
            return $this->getSuccessor()->handleRequest($property, $value);
        }
    }
}
