<?php

declare(strict_types=1);

namespace denha\Validated\Divertor;

use denha\Validated\Annotations\Rang as RangStorag;
use denha\Validated\Exception\ValidatedException;
use denha\Validated\Handle;

class Rang extends Handle
{
    /** @var RangStorag */
    private $Rang;

    public function __construct(?RangStorag $RangStorag)
    {
        $this->Rang = $RangStorag;
    }

    public function handleRequest($property, &$value)
    {
        if (!$this->Rang && null != $this->getSuccessor()) {
            return $this->getSuccessor()->handleRequest($property, $value);
        } elseif (!$this->Rang) {
            return;
        }
        
        if(null === $value || (is_string($value) && !strlen($value)) ){
            throw new ValidatedException($this->Rang->message ?: sprintf('[%s] 必须在 [%s] —— [%s] 之间 当前值为 [%s]', $property->getName(), $this->Rang->min, $this->Rang->max, $value));   
        }

        $value = (int)$value;
        if ($this->Rang->min > $value || $this->Rang->max < $value) {
            throw new ValidatedException($this->Rang->message ?: sprintf('[%s] 必须在 [%s] —— [%s] 之间 当前值为 [%s]', $property->getName(), $this->Rang->min, $this->Rang->max, $value));
        }

        // 转发给后继的责任对象
        if (null != $this->getSuccessor()) {
            return $this->getSuccessor()->handleRequest($property, $value);
        }
    }
}
