<?php

declare(strict_types=1);

namespace denha\Validated\Divertor;

use denha\Validated\Annotations\IsJsonList as IsJsonListStorag;
use denha\Validated\Exception\ValidatedException;
use denha\Validated\Handle;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\PhpParser;
use denha\Validated\Annotations\Only;
use ReflectionProperty;

class IsJsonList extends Handle
{

    /** @var array 忽略处理的关键词 */
    private $ignoreConst = ['int', 'float', 'bool', 'array', 'string'];

    /** @var IsJsonListStorag */
    private $IsJsonList;

    public function __construct(?IsJsonListStorag $IsJsonListStorag)
    {
        $this->IsJsonList = $IsJsonListStorag;
    }

    public function handleRequest($property, &$value)
    {
        if (!$this->IsJsonList && null != $this->getSuccessor()) {
            return $this->getSuccessor()->handleRequest($property, $value);
        } elseif (!$this->IsJsonList) {
            return;
        }

        if($value && is_array($value)){
            $value = json_encode($value,JSON_UNESCAPED_UNICODE);
        }

        if ($value && !is_string($value)) {
            throw new ValidatedException($this->IsJsonList->message ?: sprintf('[%s] 必须是一个Json字符串', $property->getName()));
        }
        
        if (strlen((string)$value)) {
            $tmpJsonArs = json_decode(html_entity_decode($value), true);
            if (!$tmpJsonArs) {
                throw new ValidatedException($this->IsJsonList->message ?: sprintf('[%s] 必须是一个正确的Json字符串', $property->getName()));
            } elseif ($tmpJsonArs && !is_array($tmpJsonArs)) {
                throw new ValidatedException($this->IsJsonList->message ?: sprintf('[%s] 必须是一个Json字符串同时必须是list数据类型', $property->getName()));
            }

            $this->fillValue($property, $value, $tmpJsonArs);
        }

        // 转发给后继的责任对象
        if (null != $this->getSuccessor()) {
            return $this->getSuccessor()->handleRequest($property, $value);
        }
    }

    /**
     * 填充value值
     *
     * @param ReflectionProperty $property
     * @param mixd $value
     * @param array $jsonList
     * @return void
     */
    private function fillValue(ReflectionProperty $property, &$value, array $jsonList)
    {
        $docComment = $property->getDocComment();
        if (!preg_match('#@var[\s*](\S+)[\s*](.*?)\n#', $docComment, $varMatche)) {
            return;
        }

        if ($varMatche[1] == 'array') {
            $value = $jsonList;
        }

        // 匹配 array<mixd,class-string>
        else if (preg_match('#array<\S+,(\S+)>#', $varMatche[1], $arrayMatche) || preg_match('#(\S+)\[\]#', $varMatche[1], $arrayMatche)) {
            $selfNamespaceName =  $property->getDeclaringClass()->getNamespaceName();

            $listClass = trim($arrayMatche[1]);

            if (in_array(strtolower($listClass), $this->ignoreConst)) {
                $value = $jsonList;
                return;
            }

            // 直接匹配类
            if (class_exists($listClass)) {
                $listClass = $listClass;
            }
            // 判断是否是同一命名空间下的类
            else if (class_exists($selfNamespaceName . '\\' . $listClass)) {
                $listClass = $selfNamespaceName . '\\' . $listClass;
            }
            // 判断是否是引用类
            else {
                $listClass = strtolower($listClass);
                // 获取引入文件
                $importClass = (new PhpParser())->parseUseStatements($property->getDeclaringClass());

                if (isset($importClass[$listClass])) {
                    $listClass = $importClass[$listClass];
                } else {
                    throw new ValidatedException(sprintf('当前类 %s 不能正确创建 查看是否需要 use 当前类名', $arrayMatche[1]));
                }
            }

            $data = [];

            // 校验属性是否存在重复值
            $this->verifyOnly($property, $jsonList);

            foreach ($jsonList as $key => $item) {
                if (!is_numeric($key)) {
                    throw new ValidatedException('参数解析失败 请传入正确的list结构数据 解析key不是一个正常的数字');
                }

                try {
                    $data[$key] = (new $listClass())->parseByArray($item);
                } catch (ValidatedException $e) {
                    if ($this->IsJsonList->lineError) {
                        throw new ValidatedException(sprintf('参数 %s 第 %s 个 {} 存在错误 : %s', $property->getName(), ($key + 1), $e->getMessage()), 0, $e);
                    } else {
                        throw new ValidatedException($e->getMessage(), 0, $e);
                    }
                }
            }

            $value = $data;
            unset($data);
        }
    }

    /**
     * 校验属性的属性值是否在数组中唯一
     *
     * @param ReflectionProperty $property
     * @param array $jsonList
     * @return void
     */
    private function verifyOnly(ReflectionProperty $property, array $jsonList)
    {
        if (!$this->IsJsonList->only) {
            return;
        }

        $AnnotationReader = new AnnotationReader();
        $annotations = $AnnotationReader->getPropertyAnnotations($property);

        /** @var Only[] */
        $onlyProperty = [];
        foreach ($annotations as $annotation) {
            if ($annotation instanceof Only) {
                $onlyProperty[] = $annotation;
            }
        }

        foreach ($onlyProperty as $only) {
            $data = array_column($jsonList, $only->name);
            if (count($data) != count(array_unique($data))) {
                throw new ValidatedException($only->message ? $only->message : sprintf('属性值 %s 不能存在重复值', $only->name));
            }
        }
    }
}
