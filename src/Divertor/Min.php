<?php

declare(strict_types=1);

namespace denha\Validated\Divertor;

use denha\Validated\Annotations\Min as MinStorag;
use denha\Validated\Exception\ValidatedException;
use denha\Validated\Handle;

class Min extends Handle
{

    /** @var MinStorag */
    private $Min;

    public function __construct(?MinStorag $MinStorag)
    {
        $this->Min = $MinStorag;
    }

    public function handleRequest($property, &$value)
    {
        if (!$this->Min && null != $this->getSuccessor()) {
            return $this->getSuccessor()->handleRequest($property, $value);
        } elseif (!$this->Min) {
            return;
        }

        if(null === $value || (is_string($value) && !strlen($value)) ){
            throw new ValidatedException($this->Max->message  ?: sprintf('[%s] 必须是一个数字 [%s]', $property->getName(), $this->Max->value));
        }

        $value = (float)$value;
        if ($this->Min->value > $value) {
            throw new ValidatedException($this->Min->message ?: sprintf('[%s] 必须大于等于 [%s]', $property->getName(), $this->Min->value));
        }

        // 转发给后继的责任对象
        if (null != $this->getSuccessor()) {
            return $this->getSuccessor()->handleRequest($property, $value);
        }
    }
}
