<?php

declare(strict_types=1);

namespace denha\Validated\Divertor;

use denha\Validated\Annotations\Pattern as PatternStorag;
use denha\Validated\Exception\ValidatedException;
use denha\Validated\Handle;

class Pattern extends Handle
{

    /** @var PatternStorag */
    private $Pattern;

    public function __construct(?PatternStorag $PatternStorag)
    {
        $this->Pattern = $PatternStorag;
    }

    public function handleRequest($property, &$value)
    {
        if (!$this->Pattern && null != $this->getSuccessor()) {
            return $this->getSuccessor()->handleRequest($property, $value);
        } elseif (!$this->Pattern) {
            return;
        }

        if (false == preg_match($this->Pattern->value, $value)) {
            throw new ValidatedException($this->Pattern->message ?: sprintf('[%s] 必须满足当前正则规则 %s', $property->getName(), $this->Pattern->value));
        }

        // 转发给后继的责任对象
        if (null != $this->getSuccessor()) {
            return $this->getSuccessor()->handleRequest($property, $value);
        }
    }
}
