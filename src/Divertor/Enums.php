<?php

declare(strict_types=1);

namespace denha\Validated\Divertor;

use denha\Validated\Annotations\Enums as EnumsStorag;
use denha\Validated\Exception\ValidatedException;
use denha\Validated\Handle;
use codebase\app\util\Enums\Enums as EnumsUtil;

class Enums extends Handle
{
    /** @var EnumsStorag */
    private $Enums;

    public function __construct(?EnumsStorag $EnumsStorag)
    {
        $this->Enums = $EnumsStorag;
    }

    public function handleRequest($property, &$value)
    {
        /** @var EnumsUtil */
        $EnumsClass = $this->Enums->stringClass;



        if (!$this->Enums && null != $this->getSuccessor()) {
            return $this->getSuccessor()->handleRequest($property, $value);
        } elseif (!$this->Enums) {
            return;
        }

        if (null !== $value) {
            $value = (string)$value;
            if (strlen($value) && $EnumsClass::tryFrom($value) === null) {
                $enumsLists = array_column($EnumsClass::cases()->toArray(), 'value');
                throw new ValidatedException($this->Enums->message ?: sprintf('[%s] 值必须是 [%s]', $property->getName(), implode(',', $enumsLists)));
            }
        }

        // 转发给后继的责任对象
        if (null != $this->getSuccessor()) {
            return $this->getSuccessor()->handleRequest($property, $value);
        }
    }
}
