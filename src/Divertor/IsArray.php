<?php

declare(strict_types=1);

namespace denha\Validated\Divertor;

use denha\Validated\Annotations\IsArray as IsArrayStorag;
use denha\Validated\Exception\ValidatedException;
use denha\Validated\Handle;

class IsArray extends Handle
{
    /** @var IsArrayStorag */
    private $IsArray;

    public function __construct(?IsArrayStorag $IsArrayStorag)
    {
        $this->IsArray = $IsArrayStorag;
    }

    public function handleRequest($property, &$value)
    {
        if (!$this->IsArray && null != $this->getSuccessor()) {
            return $this->getSuccessor()->handleRequest($property, $value);
        } elseif (!$this->IsArray) {
            return;
        }

        if ($value && !is_array($value)) {
            throw new ValidatedException($this->IsArray->message ?: sprintf('[%s] 必须是数组或空值', $property->getName()));
        }

        // 转发给后继的责任对象
        if (null != $this->getSuccessor()) {
            return $this->getSuccessor()->handleRequest($property, $value);
        }
    }
}
