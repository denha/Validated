<?php

declare(strict_types=1);

namespace denha\Validated\Divertor;

use denha\Validated\Annotations\Max as MaxStorag;
use denha\Validated\Exception\ValidatedException;
use denha\Validated\Handle;

class Max extends Handle
{

    /** @var MaxStorag */
    private $Max;

    public function __construct(?MaxStorag $MaxStorag)
    {
        $this->Max = $MaxStorag;
    }

    public function handleRequest($property, &$value)
    {
        if (!$this->Max && null != $this->getSuccessor()) {
            return $this->getSuccessor()->handleRequest($property, $value);
        } elseif (!$this->Max) {
            return;
        }

        if (null === $value || (is_string($value) && !strlen($value))) {
            throw new ValidatedException($this->Max->message  ?: sprintf('[%s] 必须是一个数字 [%s]', $property->getName(), $this->Max->value));
        }

        $value = (float)$value;
        if ($this->Max->value < $value) {
            throw new ValidatedException($this->Max->message  ?: sprintf('[%s] 必须小于等于 [%s]', $property->getName(), $this->Max->value));
        }

        // 转发给后继的责任对象
        if (null != $this->getSuccessor()) {
            return $this->getSuccessor()->handleRequest($property, $value);
        }
    }
}
