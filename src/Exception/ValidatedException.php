<?php

declare(strict_types=1);

namespace denha\Validated\Exception;

use codebase\app\util\Exception\ExceptionInterface;
use codebase\app\util\Log\Log;
use Exception;

/**
 * 校验异常类型
 */
class ValidatedException extends Exception implements ExceptionInterface
{
    public function __construct(string $message = "", int $code = 0)
    {
        parent::__construct($message, 40002);
    }

    function sendMessage()
    {
        header('Content-Type:application/json; charset=utf-8');
        Log::info("参数校验失败:{} code:{}", $this->getMessage(), $this->getCode());
        return json_encode(['errorcode' => $this->getCode(), 'errormsg' => $this->getMessage()], JSON_UNESCAPED_UNICODE);
    }
}
