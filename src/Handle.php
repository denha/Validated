<?php

declare(strict_types=1);

namespace denha\Validated;

use ReflectionProperty;

/**
 * 责任链
 */
abstract  class Handle
{

    /**
     * 持有后继的责任对象
     *
     * @var object
     */
    protected $successor;

    /**
     * 示意处理请求的方法
     */
    public abstract function handleRequest(ReflectionProperty $property, &$value);

    /**
     * 取值方法
     *
     * @return object
     */
    public function getSuccessor()
    {
        return $this->successor;
    }

    /**
     * 赋值方法，设置后继的责任对象
     *
     * @param object $objsuccessor            
     */
    public function setSuccessor($objsuccessor)
    {
        $this->successor = $objsuccessor;
    }
}
