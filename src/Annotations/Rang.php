<?php

declare(strict_types=1);

namespace denha\Validated\Annotations;

use Doctrine\Common\Annotations\Annotation\Required;

/**
 * 元素大小必须在指定范围
 * 
 * @Annotation
 * @Target({"PROPERTY"})
 * 
 */
class Rang
{
    /**
     * @Required()
     * @var int
     */
    public $min;

    /**
     * @Required()
     * @var int
     */
    public $max;

    /**
     * @var string
     */
    public $message;
}