<?php

declare(strict_types=1);

namespace denha\Validated\Annotations;

/**
 * 元素必须是json字符串或空 并且字符串必须是list类型
 * 
 * @Annotation
 * @Target({"PROPERTY"})
 * 
 */
class IsJsonList
{
    
    /** @var bool 是否开启唯一属性 */
    public $only = false;

    /**
     * @var bool 是否开启行内错误提示
     */
    public $lineError = false;

    /**
     * @var string
     */
    public $message;
}