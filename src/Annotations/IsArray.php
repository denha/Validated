<?php

declare(strict_types=1);

namespace denha\Validated\Annotations;

/**
 * 元素必须是数组或空
 * 
 * @Annotation
 * @Target({"PROPERTY"})
 * 
 */
class IsArray
{
    /**
     * @var string
     */
    public $message;
}