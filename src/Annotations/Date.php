<?php

declare(strict_types=1);

namespace denha\Validated\Annotations;

/**
 * 如果存在属性值 则值必须是时间戳(秒) 或是正确的日期格式
 * 同时转换成 固定日期格式 默认 YYYY-MM-DD HH:ii:ss的日期格式
 * 
 * @Annotation
 * @Target({"PROPERTY"})
 * 
 */
class Date
{

    /** @var string 日期格式 */
    public $foramt = 'Y-m-d H:i:s';

    /**
     * @var string
     */
    public $message;

    public function __construct($vals)
    {
        $this->stringClass = $vals['value'];
    }
}