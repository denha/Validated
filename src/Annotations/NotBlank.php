<?php

declare(strict_types=1);

namespace denha\Validated\Annotations;

/**
 * 判断字符串不能为null或者是空串(去掉首尾空格)
 * 
 * @Annotation
 * @Target({"PROPERTY"})
 * 
 */
class NotBlank
{
    /**
     * @var string
     */
    public $message;
}