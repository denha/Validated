<?php

declare(strict_types=1);

namespace denha\Validated\Annotations;

use Doctrine\Common\Annotations\Annotation\Required;

/**
 * 是否开启参数校验
 * 
 * @Annotation
 * @Target({"CLASS"})
 * 
 */
class Validated
{
    /**
     * @var bool
     */
    public $isOpen = true;

    public function __construct($vals)
    {
        $this->isOpen = $vals['value'];
    }
}