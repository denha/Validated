<?php

declare(strict_types=1);

namespace denha\Validated\Annotations;

/**
 * 元素必须是枚举值或者null值或者空值
 * 
 * @Annotation
 * @Target({"PROPERTY"})
 * 
 */
class Enums
{

    /** @var string-class 枚举对象 */
    public $stringClass;

    /**
     * @var string
     */
    public $message;

    public function __construct($vals)
    {
        $this->stringClass = $vals['value'];
    }
}