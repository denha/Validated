<?php

declare(strict_types=1);

namespace denha\Validated\Annotations;

use Doctrine\Common\Annotations\Annotation\Required;

/**
 * 元素值必须小于等于当前值
 * 
 * @Annotation
 * @Target({"PROPERTY"})
 * 
 */
class Max
{
    /**
     * @Required()
     * @var int
     */
    public $value;

    /**
     * @var string
     */
    public $message;
}