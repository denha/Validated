<?php

declare(strict_types=1);

namespace denha\Validated\Annotations;

use Doctrine\Common\Annotations\Annotation\Required;

/**
 * 必须满足当前正则表达式
 * 
 * @Annotation
 * @Target({"PROPERTY"})
 * 
 */
class Pattern
{
    /**
     * @Required()
     * @var string
     */
    public $value;

    /**
     * @var string
     */
    public $message;
}