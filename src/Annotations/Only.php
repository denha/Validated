<?php

declare(strict_types=1);

namespace denha\Validated\Annotations;

/**
 * 属性值唯一
 * 
 * @Annotation
 * @Target({"PROPERTY"})
 * 
 */
class Only
{
    
    /** @var string */
    public $name;

    /**
     * @var string
     */
    public $message;
}