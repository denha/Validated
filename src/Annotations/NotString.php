<?php

declare(strict_types=1);

namespace denha\Validated\Annotations;

/**
 * 元素不能是字符串
 * 
 * @Annotation
 * @Target({"PROPERTY"})
 * 
 */
class NotString
{
    /**
     * @var string
     */
    public $message;
}