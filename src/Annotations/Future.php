<?php

declare(strict_types=1);

namespace denha\Validated\Annotations;

/**
 * 必须是日期格式 并且必须是将来的日期
 * 
 * @Annotation
 * @Target({"PROPERTY"})
 * 
 */
class Future
{
    /**
     * @var string
     */
    public $message;
    
}
