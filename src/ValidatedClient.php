<?php

declare(strict_types=1);

namespace denha\Validated;

use denha\Validated\Annotations\Date as AnnotationsDate;
use denha\Validated\Annotations\Enums as AnnotationsEnums;
use denha\Validated\Annotations\Future as AnnotationsFuture;
use denha\Validated\Annotations\IsArray as AnnotationsIsArray;
use denha\Validated\Annotations\IsJsonList as AnnotationsIsJsonList;
use denha\Validated\Annotations\Max as AnnotationsMax;
use denha\Validated\Annotations\Min as AnnotationsMin;
use denha\Validated\Annotations\NotBlank as AnnotationsNotBlank;
use denha\Validated\Annotations\NotString as AnnotationsNotString;
use denha\Validated\Annotations\Past as AnnotationsPast;
use denha\Validated\Annotations\Pattern as AnnotationsPattern;
use denha\Validated\Annotations\Rang as AnnotationsRang;
use denha\Validated\Annotations\Validated;
use denha\Validated\Divertor\Date;
use denha\Validated\Divertor\Enums;
use denha\Validated\Divertor\Future;
use denha\Validated\Divertor\IsArray;
use denha\Validated\Divertor\IsJsonList;
use denha\Validated\Divertor\Max;
use denha\Validated\Divertor\Min;
use denha\Validated\Divertor\NotBlank;
use denha\Validated\Divertor\NotString;
use denha\Validated\Divertor\Past;
use denha\Validated\Divertor\Pattern;
use denha\Validated\Divertor\Rang;
use Doctrine\Common\Annotations\AnnotationReader;
use ReflectionClass;
use ReflectionProperty;

/**
 * 参数校验处理端
 */
class ValidatedClient
{

    /** @var AnnotationReader */
    private $AnnotationReader;

    /** @var array 注解使用类 */
    private $AnnotationEnums = [
        AnnotationsFuture::class => Future::class,
        AnnotationsMax::class => Max::class,
        AnnotationsMin::class => Min::class,
        AnnotationsNotBlank::class => NotBlank::class,
        AnnotationsPast::class => Past::class,
        AnnotationsPattern::class => Pattern::class,
        AnnotationsRang::class => Rang::class,
        AnnotationsNotString::class => NotString::class,
        AnnotationsIsArray::class => IsArray::class,
        AnnotationsIsJsonList::class => IsJsonList::class,
        AnnotationsEnums::class => Enums::class,
        AnnotationsDate::class => Date::class,
    ];

    public function __construct()
    {
        $this->AnnotationReader = new AnnotationReader();
    }

    /**
     * 校验是否开启了参数校验
     *
     * @param object $className
     * @return boolean
     */
    public function checkValidated(object $className): bool
    {
        $classRefection = new ReflectionClass($className);
        $Validated = $this->AnnotationReader->getClassAnnotation($classRefection, Validated::class);
        if (!$Validated || $Validated->isOpen == false) {
            return false;
        }

        return true;
    }

    /**
     * 构建动态责任链
     *
     * @param ReflectionProperty $prop
     * @return Handle
     */
    public function buildHandleByAnnotation(ReflectionProperty $prop): ?Handle
    {

        $Annotations = $this->AnnotationReader->getPropertyAnnotations($prop);

        /** @var Handle */
        $tailHandle = $handle = null;

        foreach ($Annotations as $item) {
            $annotationClassName = get_class($item);
            if (array_key_exists($annotationClassName, $this->AnnotationEnums)) {

                $divertorClassName = $this->AnnotationEnums[$annotationClassName];

                if (null == $handle) {
                    $handle = $tailHandle = new $divertorClassName($item);
                } else {
                    $tailHandle->setSuccessor(new $divertorClassName($item));
                    $tailHandle = $tailHandle->getSuccessor();
                }
            }
        }

        return $handle ?? null;
    }
}
